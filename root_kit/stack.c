/*
 * stack.c
 * 
 * Ce programme contient une vulnérabilité de type buffer overflow en lisant un fichier badfile sans vérification 
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// buffer over flow
int bof(char *str)
{
    char buffer[24];

    // La vulnérabilité est ici, utiliser strlcpy à la place de strcpy suffirait à rendre l'attaque caduque 
    strcpy(buffer, str);

    return 1;
}

// Point d'entrée du progamme
int main(int argc, char **argv)
{
    char str[517];
    FILE *badfile;

    // Lis un fichier badfile et stock son contenu directement dans str
    badfile = fopen("badfile", "r");
    fread(str, sizeof(char), 517, badfile);

    // Appel de la fonction non sécurisée avec le contenu du fichier
    bof(str);

    // Si on obtient cela, c'est que le buffer overflow a échoué (64 bits probablement)
    printf("Returned Properly\n");
    return 1;
}
