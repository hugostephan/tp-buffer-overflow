# Buffer-Overflow Vulnerability

Source: http://www.cis.syr.edu/~wedu/seed/Labs_12.04/Vulnerability/Buffer_Overflow/

Le programme illustre l'utilisation d'un buffer overflow d'un programme vulnérable pour obtenir un shell root.

## Compilation

Compiler le code qui lance le shellcode avec l'option -z execstack :

```bash
$ gcc -z execstack -o call_shellcode call_shellcode.c
```

Compiler le programme vulnérable type toujours avec -z execstack et avec -fno-stack-protector. Vous devez être connecté en tant que `root` pour mettre le `set-root-uid`.

```
$ su root 
Password (enter root password) 
# gcc -o stack -z execstack -fno-stack-protector stack.c 
# chmod 4755 stack # exit
```

Enfin, compiler le programme qui va exploiter la faille du programme vulnérable (pas d'option particulière) :

```bash
$ gcc -o exploit exploit.c 
```

## Obtenir un shell

Lancer le programme qui exploite la faille et créé le `badfile` :

```bash
$ ./exploit
```

Lancer le programme vulnérable pour initer l'attaque :

```bash
$./stack
```

Vous avez maintenant un shell root :

```
# 
``` 
