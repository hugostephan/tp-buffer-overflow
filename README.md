# tp-buffer-overflow

Le TP contient 2 exemples de buffer overflow dans le but d'ouvrir un shell, se référer aux readmes des sous dossiers pour plus de précisions.


## segment_fault

Un exemple pédagogique d'utilisation de buffer overflow segment fault pour pouvoir exécuter un shellcode qui ouvre un nouveau shell, à faire à partir de gdb.


## root_kit

Un exemple plus réaliste d'utilisation de buffer overflow détourné et plus réaliste.
