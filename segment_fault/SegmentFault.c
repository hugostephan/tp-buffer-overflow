/*
 * SegmentFault.c : Exemple de programme non protégé qui déclenche une erreur segment fault à condition d'être lancé avec l'option -fno-stack-protector
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

// Exemple de fonction d'affichage 
void unsafePrint(char *arg)
{
    char buffer[8];

    // Copie avec la version non protégé de strcpy, utiliser strlcpy suffit à empêcher l'erreur
    strcpy(buffer, arg);

    printf("%s\n", buffer);
}

// Point d'entrée du programme
int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        printf("Le sript prend une chaîne de caractère en argument ! \n");
    }
    else
    {
	setuid(0);
        unsafePrint(argv[1]);
    }

    return 0;
}


